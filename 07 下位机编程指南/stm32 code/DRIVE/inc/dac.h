#ifndef __DAC_H
#define __DAC_H	
#include "sys.h"

void DAC_QuickInit(uint16_t DAC_Channel);
void DAC_SetData(uint16_t DAC_Channel,uint16_t Data);
 
#endif 
