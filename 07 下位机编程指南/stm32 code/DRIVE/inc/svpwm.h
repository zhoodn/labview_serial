#ifndef __SVPWM_H_
#define __SVPWM_H_

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"

void Svpwm_Init(void);
void Create_Sin_Number(void);

#endif
