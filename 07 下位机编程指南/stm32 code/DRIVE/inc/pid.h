#ifndef __PID_H__
#define __PID_H__

#include "sys.h"

void PID_init(void);
s16 PID_realize(s16 Goal ,s16 Nowvalue , float p , float i ,float d);
float abs(float value);

#endif 
