#ifndef __USART_H
#define __USART_H

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_it.h"
#include "stdio.h"	
#include "string.h"

extern u8 UsartSendFlag;

void USART1_Init(u32 bound);
void send_rec(void);
void usart_send(USART_TypeDef * USARTx, char a[]);

#endif

