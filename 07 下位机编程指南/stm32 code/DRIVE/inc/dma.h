#ifndef __DMA_H
#define __DMA_H	
#include "sys.h"
#include "stm32f10x_adc.h"

#define Data_Number 1
#define Channel_Number 1

#define ADC_DMA_Channel_0 0
#define ADC_DMA_Channel_1 1
#define ADC_DMA_Channel_2 2
#define ADC_DMA_Channel_3 3
#define ADC_DMA_Channel_4 4

extern uint16_t Value[Data_Number][Channel_Number];

float Average_Value(uint16_t Which_Channel);

void Wait_DMA_ADC_done(void);

void DMA_MULTIPLE_ADC_QuickInit(void);
 
#endif 
