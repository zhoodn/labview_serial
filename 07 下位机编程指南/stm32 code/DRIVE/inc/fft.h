#ifndef __FFT_H
#define __FFT_H
#include "sys.h"

#define   NPT   1024

extern u32 lBufInArray[NPT];
extern u32 lBufOutArray[NPT];
extern u32 lBufMagArray[NPT];

void GetPowerMag(void);
void InitBufInArray(void);

#endif
