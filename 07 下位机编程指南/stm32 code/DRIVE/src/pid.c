#include "pid.h"
#include "math.h"

struct _pid
{
	//int16_t--s16
    s16 SetValue ;//设定值
    s16 ActualValue;//实际值
    s16 err;//本次偏差
    s16 last_err;//上一次偏差
    s16 integral;//积分的累加值
    float Kp;//比例系数
    float Ki;//微分系数
    float Kd;//积分系数
    float Result ;//最后的运算结果
    u8    index;//是否进行积分运算  
}pid;

float abs(float value)
{
    if(value >= 0 )
        value = value;
    else if(value < 0)
        value = -value;    
    return value;
}


//PID运算函数,位置式PID
//输入参数：目标值、实际值、P、I、D
s16 PID_realize(s16 Goal ,s16 Nowvalue , float p , float i ,float d)
{
    pid.Kp = p ;
    pid.Ki = i ;
    pid.Kd = d ;

    pid.err = Goal - Nowvalue ;//获取本次偏差
    
    if(abs(pid.err)>300)
        pid.index=0;//倒立要求
    else 
        pid.index=1;
    
    pid.integral+= pid.err*pid.index;   //积分计算
    if(pid.integral > 500 / pid.Ki)
        pid.integral = 500 / pid.Ki;
    if(pid.integral < -500 / pid.Ki)
        pid.integral = -500 / pid.Ki;
    
    //PID结果 = P*error + I*积分值 + D*微分
    pid.Result = pid.Kp * pid.err  + pid.Ki * pid.integral+ pid.Kd * (pid.err -pid.last_err);
    pid.last_err = pid.err ; //误差更新
    return (int)(pid.Result);
}

