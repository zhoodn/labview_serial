#include "dma.h"

uint16_t Value[Data_Number][Channel_Number];

float Average_Value(uint16_t Which_Channel)
{
    float Average_V;
    uint16_t Sum = 0;
    uint8_t i;
    for(i = 0; i < Data_Number; i ++)
    {
        Sum += Value[i][Which_Channel];
    }
    Average_V = Sum * 3.3 / 4096 / Data_Number;
    return Average_V;
}

void Wait_DMA_ADC_done()
{
    while(1)
    {
        if(DMA_GetFlagStatus(DMA1_FLAG_TC1)!=RESET) //等待通道1传输完成
        {
            DMA_ClearFlag(DMA1_FLAG_TC1);       //清除通道1传输完成标志
            break; 
        }
    }
}

void DMA_MULTIPLE_ADC_QuickInit(void)
{   
    DMA_InitTypeDef DMA_InitStructure;
    ADC_InitTypeDef ADC_InitStructure; 
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);
    if(Channel_Number >= 1)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_0, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 2)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_1, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 3)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_2, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 4)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_3, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 5)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_4, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 6)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_5, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 7)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_6, GPIO_Mode_AIN); 
    }
    if(Channel_Number >= 8)
    {
        GPIO_QuickInit(GPIOA, GPIO_Pin_7, GPIO_Mode_AIN); 
    }
    DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) (&(ADC1->DR));
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) & Value;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = Data_Number * Channel_Number;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);
    DMA_Cmd(DMA1_Channel1 , ENABLE);
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE ; 
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel = Channel_Number;	
    ADC_Init(ADC1, &ADC_InitStructure);
    //RCC_ADCCLKConfig(RCC_PCLK2_Div8); 
    if(Channel_Number >= 1)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 2)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 2, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 3)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 3, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 4)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 4, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 5)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 5, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 6)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 6, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 7)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_6, 7, ADC_SampleTime_28Cycles5);
    }
    if(Channel_Number >= 8)
    {
        ADC_RegularChannelConfig(ADC1, ADC_Channel_7, 8, ADC_SampleTime_28Cycles5);
    }
    ADC_DMACmd(ADC1, ENABLE);
    ADC_Cmd(ADC1, ENABLE);
    ADC_ResetCalibration(ADC1);
    while(ADC_GetResetCalibrationStatus(ADC1));
    ADC_StartCalibration(ADC1);
    while(ADC_GetCalibrationStatus(ADC1));
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

