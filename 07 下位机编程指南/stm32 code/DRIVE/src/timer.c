#include "timer.h"

void TIM_QuickInit(TIM_TypeDef * TIMx,uint32_t tim)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
    
    uint8_t channel;
    uint16_t psc = 1;
    uint32_t count = tim * 72;

	switch((uint32_t)TIMx)
    {
        case (uint32_t) TIM2: 
            channel = TIM2_IRQn; 
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
            break;
        case (uint32_t) TIM3: 
            channel = TIM3_IRQn; 
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
            break;
        case (uint32_t) TIM4: 
            channel = TIM4_IRQn; 
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
            break;
        case (uint32_t) TIM5: 
            channel = TIM5_IRQn; 
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
            break;
    }
    
    for(psc = 1; (count / psc) >= 65535; psc *= 2);

	TIM_TimeBaseStructure.TIM_Period = count / psc - 1;    //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler = psc - 1;                    //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;                 //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);              //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIMx,TIM_IT_Update,ENABLE);
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);            //设置中断优先级分组2
    
	NVIC_InitStructure.NVIC_IRQChannel = channel;              //TIMX中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;         //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;            //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);                            //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

	TIM_Cmd(TIMx, ENABLE);  //使能TIMx外设							 
}

//void TIM2_IRQHandler(void)   //TIM2中断
//{
//	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM2, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            if(key0.key_change_bit == 1)
//            {
//                printf("%.2f\n",Get_Adc_Average(ADC1,10));
//                key0.key_change_bit = 0;
//            }
//		}
//}

//void TIM3_IRQHandler(void)   //TIM3中断
//{
//	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            LED1=!LED1;
//		}
//}

//void TIM4_IRQHandler(void)   //TIM4中断
//{
//	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM4, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            LED1=!LED1;
//		}
//}

//void TIM5_IRQHandler(void)   //TIM5中断
//{
//	if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM5, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            LED0=!LED0;
//		}
//}











