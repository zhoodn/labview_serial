#include "led.h"

void LED_Init(void)
{       
    GPIO_QuickInit(GPIOA, GPIO_Pin_1, GPIO_Mode_Out_PP);
    GPIO_SetBits(GPIOA,GPIO_Pin_1);						     //PA.8 输出高

    GPIO_QuickInit(GPIOA, GPIO_Pin_2, GPIO_Mode_Out_PP);
    GPIO_SetBits(GPIOA,GPIO_Pin_2); 						 //PB.5 输出高     
    
    GPIO_QuickInit(GPIOA, GPIO_Pin_3, GPIO_Mode_Out_PP);
    GPIO_SetBits(GPIOA,GPIO_Pin_3); 						 //PB.11输出高      
}
 
