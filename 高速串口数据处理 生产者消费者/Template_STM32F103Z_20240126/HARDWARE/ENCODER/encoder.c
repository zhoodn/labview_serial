#include "encoder.h"

int32_t OverflowCNT;    //溢出次数（圈数）

void TIM_Encoder_QuickInit(TIM_TypeDef * TIMx ,uint32_t TIM_period)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_ICInitTypeDef TIM_ICInitStructure;
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
     NVIC_InitTypeDef NVIC_InitStructure;
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

    if(TIMx == TIM1)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);   
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
       
        GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8|GPIO_Pin_9; //初始化IO口
 
        GPIO_Init(GPIOA, &GPIO_InitStructure);  
    }
    if(TIMx == TIM8)
    {   
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
       
        GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6|GPIO_Pin_7; //初始化IO口;  
        GPIO_Init(GPIOC, &GPIO_InitStructure);  
    }
    
    TIM_TimeBaseStructure.TIM_Period = TIM_period; //线数×4
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure); 

	TIM_EncoderInterfaceConfig(TIMx, TIM_EncoderMode_TI12,TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_ICStructInit(&TIM_ICInitStructure);
	TIM_ICInitStructure.TIM_ICFilter = 0;//ICx_FILTER;
	TIM_ICInit(TIMx, &TIM_ICInitStructure);

	TIM_SetCounter(TIMx, 0); 
	TIM_ARRPreloadConfig(TIMx, ENABLE);

	TIM_Cmd(TIMx, ENABLE); 
	TIM_ClearFlag(TIMx, TIM_IT_Update); 
	TIM_ITConfig(TIMx, TIM_IT_Update, ENABLE);  
}

uint32_t Get_Encoder_Num(TIM_TypeDef * TIMx)
{
    return TIM_GetCounter(TIMx);
}


void TIM1_IRQHandler(void)   //TIM2中断
{
	if (TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
    {
        TIM_ClearITPendingBit(TIM1, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
       // LED1 = !LED1;
    }
}
 

