#ifndef _ENCODER_H
#define _ENCODER_H

#include "stm32f10x.h"

#define TIM1_period 1599
#define TIM8_period 1599

void TIM_Encoder_QuickInit(TIM_TypeDef * TIMx ,uint32_t TIM_period);
uint32_t Get_Encoder_Num(TIM_TypeDef * TIMx);

#endif
