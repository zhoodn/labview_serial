#ifndef __USART_H
#define __USART_H

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_it.h"
#include "stdio.h"

void USART_QuickInit(USART_TypeDef * USARTx, u32 bound);
void USART_NVIC_Config(USART_TypeDef * USARTx, unsigned char PreemptionPriority, unsigned char SubPriority);
void Usart_SendByte(USART_TypeDef * USARTx, uint8_t ch);
void Usart_SendArray(USART_TypeDef * USARTx, uint8_t *array, uint16_t len);
void Usart_SendString(USART_TypeDef * USARTx, char *str);
void Usart_SendHalfWord( USART_TypeDef * USARTx, uint16_t ch);

#endif

