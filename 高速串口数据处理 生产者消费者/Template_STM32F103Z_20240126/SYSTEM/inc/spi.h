#ifndef __SPI_H
#define __SPI_H

#include "sys.h"

void SPI_QuickInit(SPI_TypeDef * SPIx, uint16_t BaudRatePrescaler, uint16_t DataSize, uint16_t CPOL, uint16_t CPHA, uint16_t FirstBit);
uint16_t SPI_WriteReadData(SPI_TypeDef * SPIx, uint16_t dat);

#endif
