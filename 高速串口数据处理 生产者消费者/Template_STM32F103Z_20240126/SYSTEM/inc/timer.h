#ifndef __TIMER_H
#define __TIMER_H
#include "sys.h"

void TIM_QuickInit(TIM_TypeDef * TIMx, uint32_t x_us);
void TIM_NVIC_Config(TIM_TypeDef * TIMx, unsigned char PreemptionPriority, unsigned char SubPriority);
 
#endif
