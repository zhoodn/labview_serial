#include "exti.h"

/*
* Function name : EXTI_QuickInit
* Description   : 外部中断初始化
* Example       : EXTI_QuickInit(GPIOA, GPIO_Pin_0, GPIO_Mode_IN_FLOATING, EXTI_Trigger_Rising_Falling);
*/
void EXTI_QuickInit(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode, EXTITrigger_TypeDef What_EXTI_Trigger)
{
    EXTI_InitTypeDef EXTI_InitStructure;
    
    uint8_t GPIO_PortSourceGPIOx;
    uint8_t GPIO_PinSourcex;
    uint32_t EXTI_Linex;
    
    GPIO_QuickInit(GPIOx, GPIO_Pin, GPIO_Mode);
    RCC_APB2PeriphClockCmd (RCC_APB2Periph_AFIO,ENABLE);
    
    switch((uint32_t) GPIOx)
    {
        case (uint32_t)GPIOA: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOA; break;
        case (uint32_t)GPIOB: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOB; break;
        case (uint32_t)GPIOC: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOC; break;
        case (uint32_t)GPIOD: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOD; break;
        case (uint32_t)GPIOE: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOE; break;
        case (uint32_t)GPIOF: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOF; break;
        case (uint32_t)GPIOG: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOG; break;
    }
    switch(GPIO_Pin)
    {
        case GPIO_Pin_0:  GPIO_PinSourcex = GPIO_PinSource0;  EXTI_Linex = EXTI_Line0;  break;        
        case GPIO_Pin_1:  GPIO_PinSourcex = GPIO_PinSource1;  EXTI_Linex = EXTI_Line1;  break;        
        case GPIO_Pin_2:  GPIO_PinSourcex = GPIO_PinSource2;  EXTI_Linex = EXTI_Line2;  break;        
        case GPIO_Pin_3:  GPIO_PinSourcex = GPIO_PinSource3;  EXTI_Linex = EXTI_Line3;  break;        
        case GPIO_Pin_4:  GPIO_PinSourcex = GPIO_PinSource4;  EXTI_Linex = EXTI_Line4;  break;        
        case GPIO_Pin_5:  GPIO_PinSourcex = GPIO_PinSource5;  EXTI_Linex = EXTI_Line5;  break;        
        case GPIO_Pin_6:  GPIO_PinSourcex = GPIO_PinSource6;  EXTI_Linex = EXTI_Line6;  break;        
        case GPIO_Pin_7:  GPIO_PinSourcex = GPIO_PinSource7;  EXTI_Linex = EXTI_Line7;  break;                          
        case GPIO_Pin_8:  GPIO_PinSourcex = GPIO_PinSource8;  EXTI_Linex = EXTI_Line8;  break;                          
        case GPIO_Pin_9:  GPIO_PinSourcex = GPIO_PinSource9;  EXTI_Linex = EXTI_Line9;  break;                          
        case GPIO_Pin_10: GPIO_PinSourcex = GPIO_PinSource10; EXTI_Linex = EXTI_Line10; break;                          
        case GPIO_Pin_11: GPIO_PinSourcex = GPIO_PinSource11; EXTI_Linex = EXTI_Line11; break;                          
        case GPIO_Pin_12: GPIO_PinSourcex = GPIO_PinSource12; EXTI_Linex = EXTI_Line12; break;                          
        case GPIO_Pin_13: GPIO_PinSourcex = GPIO_PinSource13; EXTI_Linex = EXTI_Line13; break;                          
        case GPIO_Pin_14: GPIO_PinSourcex = GPIO_PinSource14; EXTI_Linex = EXTI_Line14; break;                          
        case GPIO_Pin_15: GPIO_PinSourcex = GPIO_PinSource15; EXTI_Linex = EXTI_Line15; break;
    }
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOx, GPIO_PinSourcex);
    
    EXTI_InitStructure.EXTI_Line = EXTI_Linex;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = What_EXTI_Trigger;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

/*
* Function name : EXTI_NVIC_Config
* Description   : 外部中断NVIC配置
* Example       : EXTI_NVIC_Config(GPIO_Pin_0, 3, 3);
*/
void EXTI_NVIC_Config(uint16_t GPIO_Pin, unsigned char PreemptionPriority, unsigned char SubPriority)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    
    switch(GPIO_Pin)
    {
        case GPIO_Pin_0:  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;    break;        
        case GPIO_Pin_1:  NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;    break;        
        case GPIO_Pin_2:  NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;    break;        
        case GPIO_Pin_3:  NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;    break;        
        case GPIO_Pin_4:  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;    break;        
        case GPIO_Pin_5:  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;  break;        
        case GPIO_Pin_6:  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;  break;        
        case GPIO_Pin_7:  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;  break;                          
        case GPIO_Pin_8:  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;  break;                          
        case GPIO_Pin_9:  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;  break;                          
        case GPIO_Pin_10: NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;                          
        case GPIO_Pin_11: NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;                          
        case GPIO_Pin_12: NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;                          
        case GPIO_Pin_13: NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;                          
        case GPIO_Pin_14: NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;                          
        case GPIO_Pin_15: NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
    }
    
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = PreemptionPriority;  //设置抢占优先级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = SubPriority;         //设置子优先级
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;            //使能 IRQ 中断
    NVIC_Init(&NVIC_InitStructure);
}

//void EXTI0_IRQHandler(void)
//{

//    
//	EXTI_ClearITPendingBit(EXTI_Line0);  //清除EXTI0线路挂起位
//}

//void EXTI9_5_IRQHandler(void)
//{			
//    if(EXTI_GetITStatus(EXTI_Line8) != RESET)
//    {
//
//        EXTI_ClearITPendingBit(EXTI_Line8);  //清除EXTI0线路挂起位
//    }
//}


//void EXTI15_10_IRQHandler(void)
//{

//    EXTI_ClearITPendingBit(EXTI_Line0);  //清除EXTI0线路挂起位
//}
