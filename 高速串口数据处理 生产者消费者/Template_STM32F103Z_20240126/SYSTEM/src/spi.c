#include "spi.h"

//SPI1 72M分频  
//SPI2 36M分频
//SPI3 36M分频

/*
* Function name : SPI_QuickInit
* Description   : SPI初始化
* Example       : SPI_QuickInit(SPI1, SPI_BaudRatePrescaler_16, SPI_DataSize_16b, SPI_CPOL_Low, SPI_CPHA_1Edge, SPI_FirstBit_MSB);
*/

void SPI_QuickInit(SPI_TypeDef * SPIx, uint16_t BaudRatePrescaler, uint16_t DataSize, uint16_t CPOL, uint16_t CPHA, uint16_t FirstBit)
{
	SPI_InitTypeDef SPI_InitStruct;
    
    if(SPIx == SPI1)
    {  
        //PA5->SCK,PA6->MISO,PA7->MOSI
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
        
        GPIO_QuickInit(GPIOA, GPIO_Pin_5, GPIO_Mode_AF_PP);
        GPIO_QuickInit(GPIOA, GPIO_Pin_7, GPIO_Mode_AF_PP);
        GPIO_QuickInit(GPIOA, GPIO_Pin_6, GPIO_Mode_IN_FLOATING);
    }
    
    if(SPIx == SPI2)
    { 
        //PB13->SCK,PB14->MISO,PB15->MOSI	
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);
        
        GPIO_QuickInit(GPIOB, GPIO_Pin_13, GPIO_Mode_AF_PP);
        GPIO_QuickInit(GPIOB, GPIO_Pin_15, GPIO_Mode_AF_PP);
        GPIO_QuickInit(GPIOB, GPIO_Pin_14, GPIO_Mode_IN_FLOATING);
    }
    
    if(SPIx == SPI3)
    { 
        //PB3->SCK,PB4->MISO,PB5->MOSI      
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3,ENABLE);

        GPIO_QuickInit(GPIOB, GPIO_Pin_3, GPIO_Mode_AF_PP);
        GPIO_QuickInit(GPIOB, GPIO_Pin_5, GPIO_Mode_AF_PP);
        GPIO_QuickInit(GPIOB, GPIO_Pin_4, GPIO_Mode_IN_FLOATING);        
    }
    
	SPI_InitStruct.SPI_Direction= SPI_Direction_2Lines_FullDuplex;//设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;       //设置SPI工作模式:  设置为主SPI
	SPI_InitStruct.SPI_DataSize = DataSize;  //设置SPI的数据大小 8位或16位帧
	SPI_InitStruct.SPI_CPOL = CPOL;          //串行同步时钟的空闲状态
	SPI_InitStruct.SPI_CPHA = CPHA;        //串行同步时钟的上升或下降沿数据被采样
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;           //NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
    SPI_InitStruct.SPI_BaudRatePrescaler = BaudRatePrescaler;  //定义波特率预分频的值
	SPI_InitStruct.SPI_FirstBit = FirstBit;  //指定数据传输从MSB位还是LSB位开始
	SPI_InitStruct.SPI_CRCPolynomial = 7;            //CRC值计算的多项式
	SPI_Init(SPIx,&SPI_InitStruct);
	
	SPI_Cmd(SPIx, ENABLE);
}


uint16_t SPI_WriteReadData(SPI_TypeDef * SPIx, uint16_t dat)
{
//	uint16_t i = 0;

    /* 当发送缓冲器空 */	
 	while(SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET)
	{
//		i++;
//		if(i > 10000)
//		{
//			return 0xFFFF;
//		}
	}
	
    /* 发送数据 */
   	SPI_I2S_SendData(SPIx, dat);
	
	/* 等待接收缓冲器为非空 */
	while(SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET)
    {
//		i++;
//		if(i > 10000)
//		{
//			return 0xFFFF;
//		}
	}
	
 	/* 将读取到的数值返回 */
 	return SPI_I2S_ReceiveData(SPIx);
}


