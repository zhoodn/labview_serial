#include "sys.h"

/***********************************************************************************************
*Description：GPIO初始化
*Example：GPIO_QuickInit(GPIOA, GPIO_Pin_0, GPIO_Mode_Out_PP);
***********************************************************************************************/
void GPIO_QuickInit(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode)
{
    GPIO_InitTypeDef GPIO_InitStruct1;
    
    switch((uint32_t)GPIOx)
    {
    case (uint32_t)GPIOA: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
        break;
    case (uint32_t)GPIOB: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
        break;
    case (uint32_t)GPIOC: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
        break;
    case (uint32_t)GPIOD: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
        break;
    case (uint32_t)GPIOE: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
        break;
    case (uint32_t)GPIOF: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
        break;
    case (uint32_t)GPIOG: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
        break;
    }
    
    if (GPIOx == GPIOB)
	{
		if ((GPIO_Pin == GPIO_Pin_3)|(GPIO_Pin == GPIO_Pin_4))
			GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	}
	if (GPIOx == GPIOA)
	{
		if (GPIO_Pin == GPIO_Pin_15)
			GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	}	
	if (GPIOx == GPIOA)
	{
		if ((GPIO_Pin == GPIO_Pin_13)|(GPIO_Pin == GPIO_Pin_14))
			GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	}

    GPIO_InitStruct1.GPIO_Mode = GPIO_Mode;
    GPIO_InitStruct1.GPIO_Pin = GPIO_Pin;
    GPIO_InitStruct1.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOx, &GPIO_InitStruct1);
}

//*****************************************************
//THUMB指令不支持汇编内联
//采用如下方法实现执行汇编指令WFI  
void WFI_SET(void)
{
	__ASM volatile("wfi");		  
}
//关闭所有中断
void INTX_DISABLE(void)
{		  
	__ASM volatile("cpsid i");
}
//开启所有中断
void INTX_ENABLE(void)
{
	__ASM volatile("cpsie i");		  
}
//设置栈顶地址
//addr:栈顶地址
__asm void MSR_MSP(u32 addr) 
{
    MSR MSP, r0 			//set Main Stack value
    BX r14
}
