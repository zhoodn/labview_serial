#include "timer.h"
#include "led.h"

/*
* Function name : TIM_QuickInit
* Description   : 定时器初始化
* Example       : TIM_QuickInit(TIM2, 500000);
*/
void TIM_QuickInit(TIM_TypeDef * TIMx, uint32_t x_us)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
   
    uint16_t psc = 1;
    uint32_t count = x_us * 72;

	switch((uint32_t)TIMx)
    {
        case (uint32_t) TIM2 : RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); break;
        case (uint32_t) TIM3 : RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); break;
        case (uint32_t) TIM4 : RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE); break;
        case (uint32_t) TIM5 : RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE); break;
    }
    
    for(psc = 1; (count / psc) >= 65535; psc *= 2);

	TIM_TimeBaseStructure.TIM_Period = count / psc - 1;    //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler = psc - 1;                    //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;                 //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    
	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);              //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

	TIM_Cmd(TIMx, ENABLE);  //使能TIMx外设							 
}

/*
* Function name : TIM_NVIC_Config
* Description   : 定时器NVIC初始化
* Example       : TIM_NVIC_Config(TIM2, 3, 2);
*/
void TIM_NVIC_Config(TIM_TypeDef * TIMx, unsigned char PreemptionPriority, unsigned char SubPriority)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    
	switch((uint32_t)TIMx)
    {
        case (uint32_t) TIM2 : NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  break;
        case (uint32_t) TIM3 : NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; break;
        case (uint32_t) TIM4 : NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn; break;
        case (uint32_t) TIM5 : NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn; break;      
    }
    
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = PreemptionPriority;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = SubPriority;   //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);                                //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
    
    TIM_ITConfig(TIMx,TIM_IT_Update,ENABLE);
}

void TIM2_IRQHandler(void)   //TIM2中断
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
       // LED1 = !LED1;
    }
}

//void TIM3_IRQHandler(void)   //TIM3中断
//{
//	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            LED1=!LED1;
//		}
//}

//void TIM4_IRQHandler(void)   //TIM4中断
//{
//	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM4, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            LED1=!LED1;
//		}
//}

//void TIM5_IRQHandler(void)   //TIM5中断
//{
//	if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET)   //检查指定的TIM中断发生与否:TIM 中断源 
//		{
//            TIM_ClearITPendingBit(TIM5, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源 
//            LED0=!LED0;
//		}
//}











