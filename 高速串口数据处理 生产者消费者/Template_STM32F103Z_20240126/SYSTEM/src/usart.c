#include "usart.h"
#include "sys.h"
#include "pwm.h"
#include "led.h"
//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
#if 1
#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
_sys_exit(int x) 
{ 
	x = x; 
} 
//重定义fputc函数 
int fputc(int ch, FILE *f)
{      
	while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
    USART1->DR = (u8) ch;      
	return ch;
}
#endif 

/*
* Function name : USART_QuickInit
* Description   : USART初始化
* Example       : USART_QuickInit(USART1, 9600);
*/
void USART_QuickInit(USART_TypeDef * USARTx, u32 bound)
{
    USART_InitTypeDef USART_InitStruct;
    
    if(USARTx == USART1) {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA \
                            | RCC_APB2Periph_AFIO, ENABLE);
        
        GPIO_QuickInit(GPIOA, GPIO_Pin_9, GPIO_Mode_AF_PP);  
        GPIO_QuickInit(GPIOA, GPIO_Pin_10, GPIO_Mode_IN_FLOATING); 
    }
    if(USARTx == USART2) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA \
                            | RCC_APB2Periph_AFIO, ENABLE);
        GPIO_QuickInit(GPIOA, GPIO_Pin_2, GPIO_Mode_AF_PP);  
        GPIO_QuickInit(GPIOA, GPIO_Pin_3, GPIO_Mode_IN_FLOATING); 
    } 
    if(USARTx == USART3) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB \
                            | RCC_APB2Periph_AFIO, ENABLE);
        GPIO_QuickInit(GPIOB, GPIO_Pin_10, GPIO_Mode_AF_PP);  
        GPIO_QuickInit(GPIOB, GPIO_Pin_11, GPIO_Mode_IN_FLOATING); 
    }
    if(USARTx == UART4) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC \
                            | RCC_APB2Periph_AFIO, ENABLE);
        GPIO_QuickInit(GPIOC, GPIO_Pin_10, GPIO_Mode_AF_PP);  
        GPIO_QuickInit(GPIOC, GPIO_Pin_11, GPIO_Mode_IN_FLOATING); 
    }
    if(USARTx == UART5) {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD \
                            | RCC_APB2Periph_AFIO, ENABLE);
        GPIO_QuickInit(GPIOC, GPIO_Pin_12, GPIO_Mode_AF_PP);  
        GPIO_QuickInit(GPIOD, GPIO_Pin_2, GPIO_Mode_IN_FLOATING); 
    }
    
    USART_InitStruct.USART_BaudRate = bound;                    //串口波特率
    USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
    USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;//收发模式
    
    USART_InitStruct.USART_WordLength = USART_WordLength_8b;    //字长为8位数据格式
    USART_InitStruct.USART_Parity = USART_Parity_No;            //一个停止位
    USART_InitStruct.USART_StopBits = USART_StopBits_1;         //无奇偶校验位   
     
    USART_Init(USARTx, &USART_InitStruct);
    USART_Cmd(USARTx, ENABLE); //使能串口
}

/*
* Function name : USART_NVIC_Config
* Description   : USART中断配置
* Example       : USART_NVIC_Config(USART1, 0, 0);
*/
void USART_NVIC_Config(USART_TypeDef * USARTx, unsigned char PreemptionPriority, unsigned char SubPriority)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    if(USARTx == USART1) {
        NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn; 
    }
    if(USARTx == USART2) {
        NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    }
    if(USARTx == USART3) {
        NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    }
    if(USARTx == UART4) {
        NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
    }
    if(USARTx == UART5) {
        NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
    }
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = PreemptionPriority; //抢占优先级0
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = SubPriority;               //子优先级为0
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //使能
    NVIC_Init(&NVIC_InitStructure);
        
    USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);  //开启串口接受中断
}

/*
* Function name : Usart_SendByte
* Description   : 发送一个字节
* Example       : Usart_SendByte(USART1, 0X00);
*/
void Usart_SendByte(USART_TypeDef * USARTx, uint8_t ch)
{
    USART_SendData(USARTx,ch);                                     /*发送一个字节数据到USART*/        
    while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);  /*等待发送数据寄存器为空*/
}
/*
* Function name : Usart_SendArray
* Description   : 发送8位的数组
*/
void Usart_SendArray(USART_TypeDef * USARTx, uint8_t *array, uint16_t len)
{
    uint8_t i;        
    for(i=0; i<len; i++)
    {
        Usart_SendByte(USARTx,array[i]);                     /*发送数组到USART*/
    }                        
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TC)==RESET); /*等待发送完成*/
}
/*
* Function name：Usart_SendString
* Description  ：发送字符串
*/
void Usart_SendString(USART_TypeDef * USARTx, char *str)
{
    unsigned int k=0;
    do 
    {
        Usart_SendByte(USARTx, *(str + k));
        k++;
    } while(*(str + k)!='\0');
    while(USART_GetFlagStatus(USARTx,USART_FLAG_TC)==RESET); /*等待发送完成*/              
}
/*
* Function name：Usart_SendHalfWord
* Description  ：发送一个16位数
*/
void Usart_SendHalfWord( USART_TypeDef * USARTx, uint16_t ch)
{
    uint8_t temp_h, temp_l;        
    temp_h = (ch&0XFF00)>>8;                /*取出高8位*/
    temp_l = ch&0XFF;                       /*取出低8位*/
    USART_SendData(USARTx, temp_h);         /*发送高8位*/
    while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);        
    USART_SendData(USARTx, temp_l);         /*发送低8位*/
    while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);        
}


/*
* Function name：USART1_IRQHandler
* Description  ：串口1中断服务程序
*/

extern uint8_t delay;
void USART1_IRQHandler(void)                	
{
    u8 Res = 0;
    
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  
    {
        Res = USART_ReceiveData(USART1);	//读取接收到的数据
        delay = Res;
                   
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);  //清除接收中断标志
    }
}

//void USART2_IRQHandler(void)
//{
//    u8 Res = 0;
//    if(USART_GetITStatus(USART2,USART_IT_RXNE)!=RESET)
//    {                
//        Res = USART_ReceiveData(USART2);
//    }
//    USART_ClearITPendingBit(USART1, USART_IT_RXNE);  //清除接收中断标志    
//}

//void USART3_IRQHandler(void)
//{
//    u8 Res = 0;
//    
//    if(USART_GetITStatus(USART3,USART_IT_RXNE)!=RESET)
//    {                
//        Res = USART_ReceiveData(USART3);
//    }
//    USART_ClearITPendingBit(USART1, USART_IT_RXNE);  //清除接收中断标志    
//}

//void UART4_IRQHandler(void)
//{   
//    u8 Res=0;

//    if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)
//    {
//        
//        Res=USART_ReceiveData(UART4);			
//    }
//    USART_ClearITPendingBit(UART4,USART_IT_RXNE);	//清楚中断标志位
//}

//void UART5_IRQHandler(void)
//{
//    u8 Res=0;
//    
//    if(USART_GetITStatus(UART5, USART_IT_RXNE) != RESET)
//    {   
//        Res=USART_ReceiveData(UART5);			   
//    }
//    USART_ClearITPendingBit(UART5,USART_IT_RXNE);	//清楚中断标志位
//}


