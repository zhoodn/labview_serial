#include "sys.h"
#include "usart.h"
#include "delay.h"
#include "timer.h"
#include "led.h"
#include "pwm.h"
#include "encoder.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "math.h"

#define SIN_N 402
float sin_number[SIN_N];

uint8_t SendBuffer[100] = {0};

uint16_t i = 0;
uint16_t j = SIN_N / 4;

uint8_t delay = 100;

void create_sin(void) {
    uint16_t i = 0;
    for(i = 0; i < SIN_N; i ++) {
        sin_number[i] = (cos(i * 3.14159 / SIN_N * 2) - cos((i + 1) * 3.14159 / SIN_N * 2));
        sin_number[i] *= (SIN_N / 3.14159 / 2);
    }
}

int main(void)
{
    delay_init();	    	 //延时函数初始化
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //中断优先级分组2，抢占优先级0-3，响应优先级0-3
    
    USART_QuickInit(USART1, 921600);
    USART_NVIC_Config(USART1, 3, 3);
    
    create_sin();
    
    while(1)
    {   
        if(delay != 0)
            delay_ms(delay);
        
        SendBuffer[0] = 0x55;
        SendBuffer[1] = 0xAA;
        memcpy(&SendBuffer[2], &sin_number[i], 4);
        memcpy(&SendBuffer[6], &sin_number[j], 4);
        SendBuffer[10] = 0xAA;
        SendBuffer[11] = 0x55;
        
        Usart_SendArray(USART1, SendBuffer, 12);
        
        i++;
        if(i == SIN_N) i = 0;
        j++;
        if(j == SIN_N) j = 0;
    }
}



